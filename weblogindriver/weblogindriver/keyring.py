import logging, gnomekeyring

_logger = logging.getLogger("weblogindriver.Keyring")

class KeyringItem(object):
    def __init__(self, account_type, username='', password=''):
        super(KeyringItem, self).__init__()        
        self.__account_type = account_type
        self.__username = username
        self.__password = password

    def get_type(self):
        return self.__account_type

    def get_username(self):
        return self.__username

    def get_password(self):
        return self.__password

    def set_type(self, account_type):
        self.__account_type = account_type

    def set_username(self, username):
        self.__username = username

    def set_password(self, password):
        self.__password = password

    def __repr__(self):
        return '{account_type=%s username=%s len(password)=%d}' % (self.__account_type, self.__username, len(self.__password))

### The keyring is a map from the tuple (type,username) to a password.
### In gnome-keyring itself we add to the tuple appname=WebLoginDriver
class Keyring:
    def __init__(self, is_singleton):
        if not is_singleton == 42:
            raise Exception("use keyring.get_keyring()")

        ### an in-memory substitute for gnome-keyring, set of KeyringItem, used
        ### when the real keyring is not available
        self.__fallback_items = set()

    def is_available(self):
        return gnomekeyring.is_available()

    # Returns a set of KeyringItem
    def get_logins(self, account_type, username):
        matches = set()

        if not self.is_available():
            for ki in self.__fallback_items:
                if ki.get_type() == account_type and \
                   ki.get_username() == username:
                    matches.add(ki)
        else:
            try:
                found = gnomekeyring.find_items_sync(gnomekeyring.ITEM_GENERIC_SECRET,
                                                     dict(appname='WebLoginDriver',
                                                          type=account_type,
                                                          username=username))
            except gnomekeyring.NoMatchError:
                found = set()
                
            for f in found:
                ki = KeyringItem(account_type=f.attributes['type'],
                                 username=f.attributes['username'],
                                 password=f.secret)
                matches.add(ki)

        return matches

    def get_password(self, account_type, username):
      logins = self.get_logins(account_type, username)
      _logger.debug("got logins: %s" % (str(logins)))
      if len(logins) > 0:
          return logins.pop().get_password()
      else:
          return None
        
    def remove_logins(self, account_type, username):
        _logger.debug("removing login (%s, %s)" % (account_type, username))
        new_fallbacks = set()
        for ki in self.__fallback_items:
            if ki.get_type() == account_type and \
                   ki.get_username() == username:
                pass
            else:
                new_fallbacks.add(ki)
                
        self.__fallback_items = new_fallbacks

        if self.is_available():
            try:   
                found = gnomekeyring.find_items_sync(gnomekeyring.ITEM_GENERIC_SECRET,
                                                     dict(appname='WebLoginDriver',
                                                          type=account_type,
                                                          username=username))
            except gnomekeyring.NoMatchError:
                found = set()
                
            for f in found:
                gnomekeyring.item_delete_sync(None, f.item_id)
  
    def store_login(self, account_type, username, password):

        if not password:
            self.remove_logins(account_type, username)
            return

        _logger.debug("storing login (%s, %s)" % (account_type, username))
        if not self.is_available():
            found = None
            for ki in self.__fallback_items:
                if ki.get_type() == account_type and \
                       ki.get_username() == username:
                    found = ki

            if found:
                found.set_password(password)
            else:
                ki = KeyringItem(account_type=account_type,
                                 username=username,
                                 password=password)
                self.__fallback_items.add(ki)

        else:  
            keyring_item_id = gnomekeyring.item_create_sync(None,
                                                            gnomekeyring.ITEM_GENERIC_SECRET,
                                                            "WebLoginDriver",
                                                            dict(appname="WebLoginDriver",
                                                                 type=account_type,
                                                                 username=username),
                                                            password, True)

keyring_inst = None
def get_keyring():
    global keyring_inst
    if keyring_inst is None:
        keyring_inst = Keyring(42)
    return keyring_inst


if __name__ == '__main__':
    ring = get_keyring()

    print ring.is_available()

    print "storing"
    ring.store_login(account_type='google', username='havoc.pennington+foo@gmail.com', password='frob')

    print "getting"
    print ring.get_logins(account_type='google', username='havoc.pennington+foo@gmail.com')

    print "done"
    
