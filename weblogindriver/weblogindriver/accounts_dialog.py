import sys, logging, urlparse, gnome

import gobject, gtk, dbus, pango

import gutil, ddm_util

_logger = logging.getLogger("weblogindriver.AccountsDialog")

DIALOG_WIDTH = 320

class Dialog(gtk.Window):      
    def __init__(self, account_types, online_accounts_service, *args, **kwargs):
        super(Dialog, self).__init__(*args, **kwargs)        
        
        self.__online_accounts_service = online_accounts_service

        # we need the proxy as well to connect to the signals
        try:
            self.__onlineaccounts_proxy = dbus.SessionBus().get_object('org.gnome.OnlineAccounts', '/onlineaccounts')
        except dbus.DBusException, e:
            _logger.error("onlineaccounts DBus service not available, can't manage accounts")
            return

        self.__account_types = account_types
        self.set_title('Accounts')
        self.set_position(gtk.WIN_POS_CENTER)

        self.__notebook = gtk.Notebook()
        self.__notebook.set_size_request(DIALOG_WIDTH, -1)
        self.__notebook.set_border_width(5)

        self.__outer_existing_accounts_vbox = gtk.VBox() 
        self.__outer_existing_accounts_vbox.set_border_width(5)

        self.__outer_new_account_vbox = gtk.VBox() 
        self.__outer_new_account_vbox.set_border_width(5)

        # make tabs take an equal amount of space
        self.__notebook.set_property("homogeneous", True)
        self.__notebook.append_page(self.__outer_existing_accounts_vbox, gtk.Label("Existing Accounts"))
        self.__notebook.append_page(self.__outer_new_account_vbox, gtk.Label("Add Account"))
        # make tabs expand horizontally
        self.__notebook.set_tab_label_packing(self.__outer_new_account_vbox, True, True, gtk.PACK_START)

        self.__existing_accounts_vbox = gtk.VBox()
        self.__outer_existing_accounts_vbox.pack_start(self.__existing_accounts_vbox, False, False) 
        self.__new_account_vbox = gtk.VBox() 
        self.__outer_new_account_vbox.pack_start(self.__new_account_vbox, False, False) 

        self.add(self.__notebook)

        self.connect('delete-event', self.__on_delete_event)
        self.connect('destroy', self.__on_destroy)

        # stuff below gets added to the "Existing Accounts" tab

        # self.__no_accounts_label = gtk.Label("No existing accounts")
        # self.__no_accounts_label.size_allocate(gtk.gdk.Rectangle(width=0, height=0)) 
        # self.__existing_accounts_vbox.pack_start(self.__no_accounts_label)

        self.__model = gtk.ListStore(gobject.TYPE_STRING)
        self.__model.set_sort_column_id(0, gtk.SORT_ASCENDING)
        self.__accounts_combo = gtk.ComboBox(self.__model)
        self.__existing_accounts_vbox.pack_start(self.__accounts_combo, True, False)
        textrender = gtk.CellRendererText()
        # If we set ellipsize mode, the text gets ellipsized both in the selection pop-out and
        # and in the selected item view. It would be ideal to have it be full length in the 
        # first case, but not in the second case. Otherwise, having it show up full length in
        # the selection pop-out, and having it truncated instead of ellipsized in the selected
        # item view doesn't look so bad.
        # textrender.set_property("ellipsize", pango.ELLIPSIZE_MIDDLE)
        self.__accounts_combo.pack_start(textrender, True)
        self.__accounts_combo.add_attribute(textrender, 'text', 0)
        self.__accounts_combo.connect('notify::active', self.__on_edited_account_changed)

        # TODO: don't display the dropdown and password entry box if there are no accounts to edit 
        self.__password_entry = gtk.Entry()
        self.__password_entry.set_visibility(False) # this sets a password mode
        self.__password_entry.connect('changed',
                                      self.__on_account_settings_changed)
        self.__password_entry.connect('activate',
                                      self.__on_account_settings_applied)

        password_vbox = gtk.VBox(spacing=2)
        self.__existing_accounts_label = gtk.Label()
        password_vbox.pack_start(self.__existing_accounts_label, True, False)    
        password_vbox.pack_end(self.__password_entry, False, False)
        self.__existing_accounts_vbox.pack_start(password_vbox, padding=5)

        box_with_buttons = gtk.HBox()         
        self.__check_box = gtk.CheckButton(label="Enabled")
        box_with_buttons.pack_start(self.__check_box, False, False, padding=5)  
        self.__existing_accounts_vbox.pack_start(box_with_buttons, False, False, padding=5)

        self.__check_box.connect('toggled',
                                 self.__on_account_settings_changed)

        self.__undo_button = gtk.Button(stock=gtk.STOCK_UNDO)
        self.__undo_button.connect('clicked',
                                   self.__on_account_settings_reset)
        self.__undo_button.set_sensitive(False)                     
        self.__undo_button.size_allocate(gtk.gdk.Rectangle(width=100))               

        self.__apply_button = gtk.Button(stock=gtk.STOCK_APPLY)
        self.__apply_button.connect('clicked',
                                   self.__on_account_settings_applied)
        self.__apply_button.set_sensitive(False)

        box_with_buttons.set_spacing(5)
        box_with_buttons.set_homogeneous(False)
        box_with_buttons.pack_end(self.__apply_button, expand=False, fill=False)
        box_with_buttons.pack_end(self.__undo_button, expand=False, fill=False)

        remove_button_box = gtk.HBox()
        self.__remove_button = gtk.Button(label="Remove Account")
        remove_image = gtk.Image()
        remove_image.set_from_stock(gtk.STOCK_REMOVE, gtk.ICON_SIZE_BUTTON)
        self.__remove_button.set_image(remove_image) 
        # self.__remove_button.set_sensitive(False)
        self.__remove_button.connect('clicked',
                                   self.__on_account_remove_clicked)
        remove_button_box.pack_end(self.__remove_button, expand=False, fill=False)
        self.__existing_accounts_vbox.pack_start(remove_button_box, False, False, padding=5)

        online_options_hbox = gtk.HBox(spacing=2)
        self.__save_online_check_box = gtk.CheckButton(label="Save Accounts Online")
        self.__save_online_check_box.set_active(self.__online_accounts_service.GetSaveOnlineFlag())
        self.__save_online_check_box.connect('toggled',
                                             self.__on_save_online_setting_changed)
        online_options_hbox.pack_start(self.__save_online_check_box, False, False)  

        self.__account_page_link_box = gtk.HBox()
        self.__account_page_link = Link()
        self.__account_page_link_connect_id = None
        self.__account_page_link_box.pack_start(self.__account_page_link, expand=False, fill=False)
    
        self.__outer_existing_accounts_vbox.pack_end(self.__account_page_link_box, False, False, padding=2) 
        self.__outer_existing_accounts_vbox.pack_end(online_options_hbox, False, False)   
        separator = gtk.HSeparator()
        self.__outer_existing_accounts_vbox.pack_end(separator, False, False)   

        # stuff below gets added to the "Add Account" tab

        self.__account_types_model = gtk.ListStore(gobject.TYPE_STRING)
        self.__account_types_model.set_sort_column_id(0, gtk.SORT_ASCENDING)
        self.__account_types_combo = gtk.ComboBox(self.__account_types_model)
        self.__new_account_vbox.pack_start(self.__account_types_combo, True, False)
        account_types_textrender = gtk.CellRendererText()
        self.__account_types_combo.pack_start(account_types_textrender, True)
        self.__account_types_combo.add_attribute(account_types_textrender, 'text', 0)

        _logger.debug("will get accounts types")
        self.__all_account_types = self.__online_accounts_service.GetAllAccountTypes()
        _logger.debug("will get accounts types 2")
        self.__user_info_types_by_full_name = {}
        self.__type_links_by_full_name = {}
     
        for (account_type, (type_full_name, user_info_type, type_link)) in self.__all_account_types.items():
            if (not self.__account_types) or account_type in self.__account_types:
                self.__account_types_model.append([type_full_name])
                self.__user_info_types_by_full_name[type_full_name] = user_info_type 
                self.__type_links_by_full_name[type_full_name] = type_link          
  
        # Setting padding to 1 here is a hack to get the content of both tabs to be aligned,
        # we'll need to change this when it will become possible to add new account types to
        # the existing dialog, for example, by adding stocks that require these account types
        padding = 1  
        if len(self.__account_types_model) > 0:
            padding = 5

        self.__username_entry = gtk.Entry()
        self.__username_entry.connect('changed',
                                      self.__on_new_username_changed)
        self.__username_entry.connect('activate',
                                      self.__on_new_username_added)

        self.__username_vbox = gtk.VBox(spacing=2)
        self.__username_link = Link()
        self.__username_link.set_size_request(DIALOG_WIDTH - 20, -1)
        self.__username_link.set_alignment(0.0, 0.5)
        self.__username_link.set_line_wrap(True)  
        self.__username_link_connect_id = None  
        self.__username_vbox.pack_start(self.__username_link, False, False)  
        self.__username_vbox.pack_end(self.__username_entry, False, False)
        self.__new_account_vbox.pack_start(self.__username_vbox, padding=padding)

        self.__add_button_box = gtk.HBox()
        self.__add_button = gtk.Button(label="Add Account")
        add_image = gtk.Image()
        add_image.set_from_stock(gtk.STOCK_ADD, gtk.ICON_SIZE_BUTTON)
        self.__add_button.set_image(add_image) 
        self.__add_button.set_sensitive(False)
        self.__add_button.connect('clicked',
                                   self.__on_new_username_added)
        self.__add_button_box.pack_end(self.__add_button, expand=False, fill=False)
        self.__new_account_vbox.pack_start(self.__add_button_box, False, False, padding=padding)   

        self.__account_types_combo.connect('notify::active', self.__on_edited_account_type_changed)
        if len(self.__account_types_model) > 0:
            self.__account_types_combo.set_active(0) 

        online_options_hbox_2 = gtk.HBox(spacing=2)
        self.__save_online_check_box_2 = gtk.CheckButton(label="Save Accounts Online")
        self.__save_online_check_box_2.set_active(self.__online_accounts_service.GetSaveOnlineFlag())
        self.__save_online_check_box_2.connect('toggled',
                                               self.__on_save_online_setting_changed)
        online_options_hbox_2.pack_start(self.__save_online_check_box_2, False, False)  

        self.__account_page_link_box_2 = gtk.HBox()
        self.__account_page_link_2 = Link()
        self.__account_page_link_2_connect_id = None
        self.__account_page_link_box_2.pack_start(self.__account_page_link_2, expand=False, fill=False)
    
        self.__outer_new_account_vbox.pack_end(self.__account_page_link_box_2, False, False, padding=2) 
        self.__outer_new_account_vbox.pack_end(online_options_hbox_2, False, False)   
        separator_2 = gtk.HSeparator()
        self.__outer_new_account_vbox.pack_end(separator_2, False, False)  

        if len(self.__account_types_model) > 0:
            self.__account_types_combo.set_active(0)
   
        self.__model_tree_iter_by_account = {}
        self.__current_account = None

        self.__update_online_links()        

        self.show_all()
        self.__set_no_accounts_state(True)
        
        if len(self.__account_types_model) == 0:
            self.__account_types_combo.hide()
            self.__username_link.hide()
            self.__username_entry.hide()
            self.__add_button.hide()
 
        self.__notebook.set_current_page(1)

        # google_accounts = self.__online_accounts_service.GetAllAccountsWithTypes(["google"])
        all_account_paths = self.__online_accounts_service.GetAllAccounts()
        if len(all_account_paths) == 0:
            # TODO: do something else
            pass
        else:
            for a_path in all_account_paths:
                self.__on_account_added(a_path)

        self.__connections = gutil.DisconnectSet()

        id = self.__onlineaccounts_proxy.connect_to_signal('AccountAdded',
                                                           self.__on_account_added)
        self.__connections.add(self.__onlineaccounts_proxy, id)
        id = self.__onlineaccounts_proxy.connect_to_signal('AccountRemoved',
                                                           self.__on_account_removed)
        self.__connections.add(self.__onlineaccounts_proxy, id)
        id = self.__onlineaccounts_proxy.connect_to_signal('SaveOnlineFlagChanged',
                                                           self.__on_save_online_flag_changed)
        self.__connections.add(self.__onlineaccounts_proxy, id)        
        id = self.__onlineaccounts_proxy.connect_to_signal('HaveOnlineUserFlagChanged',
                                                           self.__on_have_online_user_flag_changed)
        self.__connections.add(self.__onlineaccounts_proxy, id)  
        id = self.__onlineaccounts_proxy.connect_to_signal('ConnectedToServerFlagChanged',
                                                           self.__on_connected_to_server_flag_changed)
        self.__connections.add(self.__onlineaccounts_proxy, id)  
        id = self.__onlineaccounts_proxy.connect_to_signal('AccountUpdateFeedback',
                                                           self.__on_account_update_feedback)
        self.__connections.add(self.__onlineaccounts_proxy, id)        

    def __open_server_page(self, l, page):
        gnome.url_show(urlparse.urljoin(ddm_util.get_baseurl(), page))

    def __open_page(self, l, page):
        gnome.url_show(page)

    def __update_online_links(self):
        save_online_flag = self.__online_accounts_service.GetSaveOnlineFlag()
        have_online_user_flag = self.__online_accounts_service.GetHaveOnlineUserFlag()  
        connected_to_server_flag = self.__online_accounts_service.GetConnectedToServerFlag()  

        if self.__account_page_link_connect_id is not None:
            self.__account_page_link.disconnect(self.__account_page_link_connect_id)

        if self.__account_page_link_2_connect_id is not None:
            self.__account_page_link_2.disconnect(self.__account_page_link_2_connect_id)

        if save_online_flag and have_online_user_flag and connected_to_server_flag:
            # display a link to the account page
            self.__account_page_link.set_text("Visit Account Page")
            self.__account_page_link_connect_id = self.__account_page_link.connect("clicked", self.__open_server_page, "/account")
            self.__account_page_link_2.set_text("Visit Account Page")
            self.__account_page_link_2_connect_id = self.__account_page_link_2.connect("clicked", self.__open_server_page, "/account")
        elif save_online_flag and not have_online_user_flag:
            # tell the user that they need to sign in
            # connected_to_server_flag will always be False when we don't have a user, so we need to check this case first
            # we can't tell if the server is actually available or not in the case we don't have a user, but that's ok  
            self.__account_page_link.set_text("Click Here to Sign in", "red")
            self.__account_page_link_connect_id = self.__account_page_link.connect("clicked", self.__open_server_page, "/who-are-you")
            self.__account_page_link_2.set_text("Click Here to Sign in", "red")
            self.__account_page_link_2_connect_id = self.__account_page_link_2.connect("clicked", self.__open_server_page, "/who-are-you")
        elif save_online_flag and not connected_to_server_flag:
            # tell the user that the server is not available (either it's down or they are not online)
            self.__account_page_link.set_text("The Server is Not Available")
            self.__account_page_link_connect_id = self.__account_page_link.connect("clicked", self.__open_server_page, "")
            self.__account_page_link_2.set_text("The Server is Not Available")
            self.__account_page_link_2_connect_id = self.__account_page_link_2.connect("clicked", self.__open_server_page, "")
        else:
            # display a Learn More link 
            self.__account_page_link.set_text("Learn More")
            self.__account_page_link_connect_id = self.__account_page_link.connect("clicked", self.__open_server_page, "/accounts-learnmore")
            self.__account_page_link_2.set_text("Learn More")
            self.__account_page_link_2_connect_id = self.__account_page_link_2.connect("clicked", self.__open_server_page, "/accounts-learnmore") 

    def __on_save_online_flag_changed(self, new_save_online_flag):
        self.__save_online_check_box.set_active(new_save_online_flag)
        self.__save_online_check_box_2.set_active(new_save_online_flag)
        self.__update_online_links()

    def __on_have_online_user_flag_changed(self, new_have_online_user_flag):
        self.__update_online_links() 

    def __on_connected_to_server_flag_changed(self, new_connected_to_server_flag):
        self.__update_online_links() 

    def __on_account_added(self, a_path):
        if a_path not in self.__model_tree_iter_by_account:
            a = self.__online_accounts_service.get_existing_account(a_path)
            if a is None:
                _logger.error("onlineaccount for path %s was not found" % a_path)
                return

            if (self.__account_types and a.GetType() not in self.__account_types) or a.GetType() not in self.__all_account_types:
                return 

            _logger.debug("account added %s" % a) 
            tree_iter = self.__model.append([self.__all_account_types[a.GetType()][0] + ': ' + a.GetUsername()])
                
            self.__model_tree_iter_by_account[a_path] = tree_iter

            if len(self.__model_tree_iter_by_account) == 1:
                self.__set_no_accounts_state(False)
                self.__current_account = a
                self.__accounts_combo.set_active(0) 
                self.__notebook.set_current_page(0)     
     
    def __on_account_removed(self, a_path):
        # we don't want to remove accounts that were disabled, but are still present in GConf here
        # if accts.has_account(a):
        #     return 
 
        _logger.debug("account removed %s" % a_path) 
        if a_path in self.__model_tree_iter_by_account:
            _logger.debug("will remove")
            self.__model.remove(self.__model_tree_iter_by_account[a_path])
            del self.__model_tree_iter_by_account[a_path]
            # TODO: possibly can get the index of the deleted selection, and move to the next one 
            if self.__current_account.GetObjectPath() == a_path and len(self.__model_tree_iter_by_account) > 0:  
                new_current_account_path = self.__model_tree_iter_by_account.keys()[0]
                self.__current_account = self.__online_accounts_service.get_existing_account(new_current_account_path)
                if self.__current_account is not None:   
                    self.__accounts_combo.set_active_iter(self.__model_tree_iter_by_account.values()[0]) 
                else:
                    _logger.error("onlineaccount for path %s was not found" % new_current_account_path)
   
        if len(self.__model_tree_iter_by_account) == 0:   
            self.__set_no_accounts_state(True)

    def __set_no_accounts_state(self, no_accounts):
        if no_accounts:
            self.__accounts_combo.hide()
            self.__password_entry.hide()
            self.__check_box.hide()
            self.__undo_button.hide()
            self.__apply_button.hide()
            self.__remove_button.hide()
            # self.__account_page_link_box.hide()
            self.__existing_accounts_label.set_text("No existing accounts") 
            self.__existing_accounts_label.set_alignment(0.5, 0.0)
        else:
            self.__existing_accounts_vbox.show_all()
            self.__existing_accounts_label.set_text("Password:")
            self.__existing_accounts_label.set_alignment(0.0, 0.5)    

    def __on_edited_account_type_changed(self, *args):
        new_iter = self.__account_types_combo.get_active_iter()
        _logger.debug("account type changed to %s" % self.__account_types_model.get_value(new_iter, 0))
        if self.__username_link_connect_id is not None:
            self.__username_link.disconnect(self.__username_link_connect_id)

        self.__username_link.set_text(self.__user_info_types_by_full_name[self.__account_types_model.get_value(new_iter, 0)] + ":")
        self.__username_link_connect_id = self.__username_link.connect("clicked", self.__open_page, self.__type_links_by_full_name[self.__account_types_model.get_value(new_iter, 0)])

    def __on_edited_account_changed(self, *args):
        new_iter = self.__accounts_combo.get_active_iter()
        _logger.debug("account changed to %s" % self.__model.get_value(new_iter, 0))        
        for (a_path, tree_iter) in self.__model_tree_iter_by_account.items():
            # this assumes that text for each entry in the drop down box is different, which it should be       
            if self.__model.get_value(tree_iter, 0) == self.__model.get_value(new_iter, 0):              
                self.__current_account =  self.__online_accounts_service.get_existing_account(a_path)
                if self.__current_account is None:
                    _logger.error("onlineaccount for path %s was not found" % a_path)
                    return
                # TODO: this will trigger __on_password_entry_changed, make sure that is harmless
                self.__password_entry.set_text(self.__current_account.GetPassword())
                self.__check_box.set_active(self.__current_account.GetEnabled())
                return
        _logger.error("new edited account was not found in self.__model_tree_iter_by_account")

    def __on_account_settings_changed(self, widget):
        if self.__password_entry.get_text().strip() != self.__current_account.GetPassword() or \
           self.__check_box.get_active() != self.__current_account.GetEnabled():
            self.__undo_button.set_sensitive(True)  
            self.__apply_button.set_sensitive(True)  
        else:
            self.__undo_button.set_sensitive(False)  
            self.__apply_button.set_sensitive(False)  

    def __on_save_online_setting_changed(self, widget):
       # it's important to use the passed in widget here because we have two checkboxes (one in each tab)
       # that call this function
       if widget.get_active() != self.__online_accounts_service.GetSaveOnlineFlag():
           self.__online_accounts_service.SetSaveOnlineFlag(widget.get_active())

    def __on_account_settings_applied(self, widget):
        text = self.__password_entry.get_text()
        enabled = self.__check_box.get_active()
        self.__online_accounts_service.SaveAccountChanges(self.__current_account.GetObjectPath(),
                                                       { 'password' : str(text), 'enabled' : str(enabled) })
        self.__undo_button.set_sensitive(False)  
        self.__apply_button.set_sensitive(False)  

    def __on_account_settings_reset(self, widget):
        self.__password_entry.set_text(self.__current_account.GetPassword())
        self.__check_box.set_active(self.__current_account.GetEnabled())

    def __on_account_update_feedback(self, feedback_header, feedback_message, is_error_flag):
        message_dialog = None
        if is_error_flag: 
            message_dialog = gtk.MessageDialog(parent=self, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=feedback_header)
        else:
            message_dialog = gtk.MessageDialog(parent=self, type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_OK, message_format=feedback_header)
            
        message_dialog.set_default_response(gtk.BUTTONS_OK)
        message_dialog.format_secondary_text(feedback_message)
        message_dialog.run()
        message_dialog.destroy()

    def account_remove_return_cb(self):
        pass

    def account_remove_error_cb(self, error):
        self.__on_account_update_feedback("Could not remove an account", error, True)     

    def __on_account_remove_clicked(self, widget):
        self.__online_accounts_service.RemoveAccount(self.__current_account.GetObjectPath(), self.account_remove_return_cb, self.account_remove_error_cb)

    def __on_new_username_changed(self, widget):
        # in the future can check if the input is of the desired form here
        username_entered = (len(self.__username_entry.get_text().strip()) > 0)  
        self.__add_button.set_sensitive(username_entered)        

    # account_tuple consists of object_path, new_account_flag, feedback_message
    def account_add_return_cb(self, account_tuple):
        object_path = account_tuple[0]
        new_account_flag = account_tuple[1]
        feedback_message = account_tuple[2]
        
        if (feedback_message is not None and len(feedback_message) > 0) or (object_path is None and not new_account_flag):
            if object_path is None and (not new_account_flag): 
                # feedback_message is an error
                self.__on_account_update_feedback("Could not add an account", feedback_message, True)
            else:
                # feedback_message is a just there to provide extra info
                self.__username_entry.set_text("")
                self.__on_account_update_feedback("Information note", feedback_message, False)
        else: 
            self.__username_entry.set_text("")
            self.__notebook.set_current_page(0)
                
        # we set the newly added account to be an active iter even if we are not switching to the existing accounts view
        # because we are showing a warning 
        if object_path is not None: 
            if self.__model_tree_iter_by_account.has_key(object_path):
                # the key will already exist only if the account already existed (new_account_flag should be False in that case)
                # we could show a message to the user about it, but it is probably more helpful to just show them that account
                # in the existing accounts view
                account_iter = self.__model_tree_iter_by_account[object_path]                
                self.__accounts_combo.set_active_iter(account_iter)
            else:
                # acounts system will emit a signal that will cause __on_account_added to be called again, 
                # but it's ok to call this from here so that we can switch to the new account in the combo box right away
                self.__on_account_added(object_path)
                account_iter = self.__model_tree_iter_by_account[object_path]   
                self.__accounts_combo.set_active_iter(account_iter)

    def account_add_error_cb(self, error):
        pass

    def __on_new_username_added(self, widget):
        text = self.__username_entry.get_text()
        username_entered = (len(text.strip()) > 0)
        # don't do anything if the user pressed enter in the username textbox while it is empty 
        if not username_entered:
            return
        
        current_iter = self.__account_types_combo.get_active_iter()
        for (account_type, (type_full_name, user_info_type, type_link)) in self.__all_account_types.items():
            if self.__account_types_model.get_value(current_iter, 0) == type_full_name:
                self.__online_accounts_service.GetOrCreateAccount(account_type, text, self.account_add_return_cb, self.account_add_error_cb)
                return 
                
        _logger.warn("Did not find an account type that matched the account type in the dropdown %s" % self.__account_types_model.get_value(current_iter, 0))

    def __on_delete_event(self, dialog, event):
        self.hide()
        return True

    def __on_destroy(self, dialog):
        self.hide()
        self.__connections.disconnect_all()
        
class Link(gtk.EventBox):
    __gsignals__ = {
        "clicked" : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
    }
    
    def __init__(self,**kwargs):
        super(Link, self).__init__(**kwargs)
        self.__text = None
        self.__enabled = True
        self.__label = gtk.Label()
        self.add(self.__label)
        self.connect("button-press-event", self.__on_button_press)
        self.connect("enter_notify_event", self.__on_enter) 
        self.connect("leave_notify_event", self.__on_leave) 
        self.add_events(gtk.gdk.BUTTON_PRESS_MASK
                        & gtk.gdk.ENTER_NOTIFY_MASK
                        & gtk.gdk.LEAVE_NOTIFY_MASK)

    def set_size_request(self, width, height):
        self.__label.set_size_request(width, height)

    def set_alignment(self, x, y):
        self.__label.set_alignment(x, y)
        
    def set_ellipsize(self, do_ellipsize):
        self.__label.set_ellipsize(do_ellipsize)

    def set_line_wrap(self, do_line_wrap):
        self.__label.set_line_wrap(do_line_wrap)

    def __on_button_press(self, self2, e):
        if e.button == 1 and self.__enabled:
            self.emit("clicked")
            return True
        return False

    def get_text(self):
        return self.__text
    
    def set_text(self, text, color="blue"):
        self.__text = text
        self.set_markup(gobject.markup_escape_text(text), color)
    
    def set_enabled(self, enabled):        
        self.__enabled = enabled
        self.set_markup(gobject.markup_escape_text(self.__text))

    def set_markup(self, text, color="blue"):
        if  self.__enabled: 
            self.__label.set_markup('<span foreground="%s">%s</span>' % (color, text,))
        else:
            self.__label.set_markup('<span foreground="#666666">%s</span>' % (text,))

    def __on_enter(self, w, c):
        self.__talk_to_the_hand(True)

    def __on_leave(self, w, c):
        self.__talk_to_the_hand(False)

    def __talk_to_the_hand(self, hand):
        display = self.get_display()
        cursor = None
        if hand and self.__enabled:
            cursor = gtk.gdk.Cursor(display, gtk.gdk.HAND2)
        self.window.set_cursor(cursor)
