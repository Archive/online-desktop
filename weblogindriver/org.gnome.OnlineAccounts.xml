<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"
"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">
<node name="/" xmlns:doc="http://www.freedesktop.org/dbus/1.0/doc.dtd">
  <interface name="org.gnome.OnlineAccounts">
    <doc:doc>
      <doc:description>
	<doc:para>
          Online Accounts service can be used by desktop applications 
          to create and retrieve information about user's online accounts,
          such as Google or Twitter. Online Accounts service stores account 
          information in GConf and passwords in 
          GNOME Keyring and takes care of all GConf and GNOME Keyring                 
          manipulations. In addition, it provides applications with a 
          stock dialog that allows adding and modifying accounts of certain 
          types used by the application. The central management of accounts 
          allows the user to 
          only enter account information once and have it be available
          to all applications that work with a given account type. 
       </doc:para>
       <doc:para>
         The presence of an account type in <doc:ulink url="http://online.gnome.org/account-types">
         the list of online accounts on the GNOME Online site</doc:ulink> 
         should be ensured by the application developers. 
         This enables users who use GNOME Online to have their 
         accounts of these types available to them on any machine they use.
         Users can opt-in to use the GNOME Online service by selecting      
         "Save Accounts Online" option in the stock dialog and logging in 
         to the GNOME Online site. <doc:ulink url="http://online.gnome.org/accounts-learnmore">
         This page</doc:ulink> has an explanation about the service for
         the users.
       </doc:para>
       <doc:para>
         If the account type needed by a certain application doesn't already exist on
         the GNOME Online site, the application developers can experiment with the Online 
         Accounts service by adding the account type locally with 
         <doc:ref type="method" to="OnlineAccounts.EnsureAccountType">EnsureAccountType()
         </doc:ref>, and only adding the account type online and making it public once their
         feature is publicly available. Beyond the initial experimentation, 
         <doc:ref type="method" to="OnlineAccounts.EnsureAccountType">
         EnsureAccountType()</doc:ref> should always be used by applications to ensure 
         that Online Accounts service has information about the type even if the server 
         is not available. 
       </doc:para>
       <doc:para>
         Typically, applications should use <doc:ref type="method" to="OnlineAccounts.GetEnabledAccountsWithTypes">
         GetEnabledAccountsWithTypes()</doc:ref> to get all enabled accounts for a certain 
         set of types, as well as connect to <doc:ref type="signal" to="OnlineAccounts::AccountEnabled">
         AccountEnabled()</doc:ref> and <doc:ref type="signal" to="OnlineAccounts::AccountDisabled">
         AccountDisabled()</doc:ref> signals. Using enabled accounts and allowing the user to edit accounts
         in the stock dialog by calling 
         <doc:ref type="method" to="OnlineAccounts.OpenAccountsDialogWithTypes">OpenAccountsDialogWithTypes()
         </doc:ref> allows applications to completely delegate online accounts management to the Online 
         Accounts service. Examples of such use of the Online Accounts service can be found in <doc:ulink url="http://svn.gnome.org/viewvc/bigboard/trunk/bigboard">the code for the
         Online Desktop sidebar</doc:ulink> and in <doc:ulink url="https://twitux.svn.sourceforge.net/svnroot/twitux/trunk/src/twitux-app.c ">the code for Twitux</doc:ulink>.
       </doc:para>
       <doc:para>
         Most other functions are only useful if the application needs to have 
         its own user interface for adding and modifying accounts. If the application
         is using <doc:ref type="method" to="OnlineAccounts.GetAllAccounts">GetAllAccounts()</doc:ref>, 
         it should check if the accounts it gets are disabled.
         It should add accounts to the service with <doc:ref type="method" to="OnlineAccounts.GetOrCreateAccount">GetOrCreateAccount()</doc:ref>, save account changes
         with <doc:ref type="method" to="OnlineAccounts.SaveAccountChanges">SaveAccountChanges()</doc:ref>, and remove accounts with <doc:ref type="method" to="OnlineAccounts.RemoveAccount">RemoveAccount()</doc:ref>. It should make 
         it clear to the user that these changes will be saved centrally, and also provide the
         user with an option to save accounts on GNOME Online and use <doc:ref type="method" to="OnlineAccounts.SetSaveOnlineFlag">SetSaveOnlineFlag()</doc:ref> to
         toggle that option. <doc:ulink url="http://svn.gnome.org/viewvc/online-desktop/trunk/weblogindriver/weblogindriver/accounts_dialog.py?view=markup">Implementation of the stock accounts dialog</doc:ulink> might be useful 
         for understanding how to implement a custom user interface that uses the Online Accounts
         service to store away the accounts information centrally.
       </doc:para>
      </doc:description>
    </doc:doc>
    <signal name="AccountAdded">
      <arg type="o" name="account_object_path" >
       <doc:doc>
          <doc:summary>
            An object path to the added account.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when an account gets added. Be sure to check if the type of the
            added account is used by the application when handling this signal. Normally, 
            applications should connect to the <doc:ref type="signal" to="OnlineAccounts::AccountEnabled">
            AccountEnabled()</doc:ref> signal instead to avoid being notified about additions 
            of disabled accounts. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <signal name="AccountRemoved">
      <arg type="o" name="account_object_path" >
       <doc:doc>
          <doc:summary>
            An object path to the removed account.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when an account gets removed. Normally, 
            applications should connect to the <doc:ref type="signal" to="OnlineAccounts::AccountDisabled">
            AccountDisabled()</doc:ref> signal instead to be sure to be notified when accounts get disabled.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <signal name="AccountEnabled">
      <arg type="o" name="account_object_path" >
       <doc:doc>
          <doc:summary>
            An object path to the enabled account.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when an account gets enabled. Be sure to check if the type of the
            enabled account is used by the application when handling this signal. This is the signal 
            applications should connect to if they are only handling enabled accounts. Such applications 
            don't need to connect to the <doc:ref type="signal" to="OnlineAccounts::AccountAdded">
            AccountAdded()</doc:ref> signal as AccountEnabled() is emitted when an enabled account is 
            added as well. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <signal name="AccountDisabled">
      <arg type="o" name="account_object_path" >
        <doc:doc>
          <doc:summary>
            An object path to the disabled account.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when an account gets disabled. This is the signal applications should 
            connect to if they are only handling enabled accounts. Such applications don't need to connect to
            the <doc:ref type="signal" to="OnlineAccounts::AccountRemoved">AccountRemoved()</doc:ref> 
            signal as AccountDisabled() is emitted when an account is removed as well. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <method name="SetSaveOnlineFlag">
      <arg direction="in"  type="b" name="save_online_flag" >
        <doc:doc>
          <doc:summary>
            A boolean flag specifying if the user wants the accounts information to be saved on the GNOME Online service.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Sets a boolean flag specifying if the user wants the accounts information to be saved on 
            the GNOME Online service. This preference should be provided by the user. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetSaveOnlineFlag">
      <arg direction="out" type="b" name="save_online_flag" >
        <doc:doc>
          <doc:summary>
            Returns a boolean flag specifying if the user wants the accounts information to be saved on the GNOME Online service.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Returns a boolean flag specifying if the user wants the accounts information to be saved on the GNOME Online service.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <signal name="SaveOnlineFlagChanged">
      <arg type="b" name="new_save_online_flag" >
        <doc:doc>
          <doc:summary>
            A new boolean value specifying if the user wants the accounts information to be saved on the GNOME Online service.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when the value for a boolean flag specifying if the user wants 
            the accounts information to be saved on the GNOME Online service is changed. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <method name="GetHaveOnlineUserFlag">
      <arg direction="out" type="b" name="have_online_user_flag" >
        <doc:doc>
          <doc:summary>
            Returns TRUE if the GNOME Online identity of the desktop user is known. 
          </doc:summary> 
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
	    Indicates if the GNOME Online identity of the desktop user is known. 
            This flag is only useful if the application is not using the stock 
            accounts dialog and needs to provide the user with an ability to 
            log in to GNOME Online. 
          </doc:para>
        </doc:description>
      </doc:doc>
    </method>
    <signal name="HaveOnlineUserFlagChanged">
      <arg type="b" name="new_have_online_user_flag" >
        <doc:doc>
          <doc:summary>
            A new boolean value specifying if the GNOME Online identity of the desktop user is known.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when the value for a boolean flag specifying if the GNOME Online 
            identity of the desktop user is known is changed. This signal is only useful if the 
            application is not using the stock accounts dialog and needs to provide the user with 
            an ability to log in to GNOME Online. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <method name="GetConnectedToServerFlag">
      <arg direction="out" type="b" name="connected_to_server_flag" >
        <doc:doc>
          <doc:summary>
            Returns TRUE if the Online Accounts service is connected to the GNOME Online server. 
          </doc:summary> 
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
	    Indicates if the Online Accounts service is connected to the GNOME Online server.
          </doc:para>
        </doc:description>
      </doc:doc>
    </method>
    <signal name="ConnectedToServerFlagChanged">
      <arg type="b" name="new_connected_to_server_flag" >
        <doc:doc>
          <doc:summary>
            A new boolean value specifying if the Online Accounts service is connected to the GNOME Online server.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when the value for a boolean flag specifying if the Online Accounts service 
            is connected to the GNOME Online server is changed. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <signal name="AccountUpdateFeedback">
      <arg type="s" name="feedback_header" >
        <doc:doc>
          <doc:summary>
            A header for the feedback message.
          </doc:summary>
        </doc:doc>
      </arg>    
      <arg type="s" name="feedback_message" >
        <doc:doc>
          <doc:summary>
            A text for the feedback message.
          </doc:summary>
        </doc:doc>
      </arg>    
      <arg type="b" name="is_error_flag" >
        <doc:doc>
          <doc:summary>
            A boolean value indicating if the feedback message is an error. 
            TRUE if it is an error, and FALSE if it is just a notice.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            A signal emitted when some feedback about an online account becomes available from the server. 
            For example, this could happen if the user enabled storing accounts online, and some accounts
            that were previously stored locally failed server validation. If an application is not using
            a stock dialog, it needs to be able to display such feedback to the user. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </signal>
    <method name="SaveAccountChanges">
      <arg direction="in"  type="o" name="account_object_path" >
        <doc:doc>
          <doc:summary>
            An object path to the account for which new properties are being set.
          </doc:summary>
        </doc:doc>
      </arg>    
      <arg direction="in"  type="a{ss}" name="new_properties" >
        <doc:doc>
          <doc:summary>
            A dictionary of key and value pairs for account properties. 
            Valid values for keys are "password" and "enabled". This method needs to be
            used by an application only if it provides its own user interface for entering account
            passwords or changing account enabled status.
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Sets new values for the specified properties of the account.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetOrCreateAccount">
      <arg direction="in"  type="s" name="account_type" >
        <doc:doc>
          <doc:summary>
            A short name for the account type.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="username" >
        <doc:doc>
          <doc:summary>
            A username for the account.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="return_cb" >
        <doc:doc>
          <doc:summary>
            A function called with the return value in case of success.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="error_cb" >
        <doc:doc>
          <doc:summary>
            A function called with an error message in case of failure.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="out" type="(obs)" name="result_account_tuple" >
        <doc:doc>
          <doc:summary>
            Returns a tuple with an account object path as the first element, 
            a boolean indicating if a new account was created as a second element, 
            and a feedback message as a third element
          </doc:summary>
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
            An asynchronous function that gets or creates an account of a given type with a given username. 
            The result is returned by calling a return or an error callback function. The function is asynchronous
            because it can require communication with the GNOME Online server if a new account is added and
            the GNOME Online user identity is known.    
          </doc:para>
          <doc:para>
            Applications should not need to use this function if they use the stock account dialog. However, they
            might want to use this function to add the accounts already saved by the user through the application's
            own user interface. In this case, the application should ask the user's permission to do so first.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="RemoveAccount">
      <arg direction="in"  type="o" name="account_object_path" > 
        <doc:doc>
          <doc:summary>
            An object path for the account to be removed.
          </doc:summary>
        </doc:doc>
      </arg>  
      <arg direction="in"  type="s" name="return_cb" >
        <doc:doc>
          <doc:summary>
            A function called in case of success.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="error_cb" >
        <doc:doc>
          <doc:summary>
            A function called with an error message in case of failure.
          </doc:summary>
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
            An asynchronous function that removes the specified account. The function is asynchronous
            because it requires communication with the GNOME Online server to remove the account
            if the GNOME Online user identity is known. Applications should not need to use this function 
            if they use the stock account dialog.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetEnabledAccounts">
      <arg direction="out" type="ao" name="enabled_accounts" >
        <doc:doc>
          <doc:summary>
            Returns a list of object paths to the enabled account objects of all types. 
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Returns a list of object paths to the enabled account objects of all types. This function 
            should be used by applications to avoid getting disabled accounts. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetEnabledAccountsWithTypes">
      <arg direction="in"  type="as" name="account_types" >
        <doc:doc>
          <doc:summary>
            A list of account types for which enabled accounts should be returned.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="out" type="ao" name="enabled_accounts" >
        <doc:doc>
          <doc:summary>
            Returns a list of object paths to the enabled account objects that have specified types. 
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Returns a list of object paths to the enabled account objects that have specified types. 
            This function should be used by applications to avoid getting disabled accounts. 
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetAllAccounts">
      <arg direction="out" type="ao" name="accounts" >
        <doc:doc>
          <doc:summary>
            Returns a list of object paths to the account objects of all types. 
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Returns a list of object paths to the account objects of all types. 
            <doc:ref type="method" to="OnlineAccounts.GetEnabledAccounts">
            GetEnabledAccounts()</doc:ref> should normally be used instead to avoid 
            getting disabled accounts.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetAllAccountsWithTypes">
      <arg direction="in"  type="as" name="account_types" >
        <doc:doc>
          <doc:summary>
            A list of account types for which accounts should be returned.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="out" type="ao" name="accounts">
        <doc:doc>
          <doc:summary>
            Returns a list of object paths to the account objects that have specified types. 
          </doc:summary>
        </doc:doc>
      </arg>    
      <doc:doc>
	<doc:description>
	  <doc:para>
            Returns a list of object paths to the account objects that have specified types. 
            <doc:ref type="method" to="OnlineAccounts.GetEnabledAccountsWithTypes">
            GetEnabledAccountsWithTypes()</doc:ref> should normally be used instead to avoid 
            getting disabled accounts.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="GetAllAccountTypes">
      <arg direction="out" type="a{s(sss)}" name="account_types" >
        <doc:doc>
          <doc:summary>
            Returns a dictionary with short account names as keys and tuples of 
            account full name, account user info type, and account type site link as values.
          </doc:summary>
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
            Returns a list of account types known to the Online Accounts service.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>    
    <method name="EnsureAccountType">
      <arg direction="in"  type="s" name="type_name" >
        <doc:doc>
          <doc:summary>
            A short name for the account type, which can only contain lower 
            case letters or underscores, e.g. "twitter". This name should match 
            the short name that is listed or will be listed on 
            <doc:ulink url="http://online.gnome.org/account-types">GNOME Online 
            </doc:ulink> for the account.  
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="type_full_name" >
        <doc:doc>
          <doc:summary>
            A full name for the account type, e.g. "Twitter".
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="type_user_info_type" >
        <doc:doc>
          <doc:summary>
            A type of information that the user needs to enter to identify this account, e.g. "Twitter username".
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="s" name="type_link" >
        <doc:doc>
          <doc:summary>
            A link to a web site that can provide the user with this information, e.g. "twitter.com". 
          </doc:summary>
        </doc:doc>  
      </arg>
     <doc:doc>
	<doc:description>
	  <doc:para>
	    This method should be used to ensure that the account type is known to the
            Online Accounts service in case the GNOME Online server that provides a 
            listing of accounts is not available or the account type is not yet available
            from the server because support for it is under development and it should
            not be available publicly. This method should always be called on application
            start-up for each account type the application works with.
          </doc:para>
        </doc:description>
      </doc:doc> 
    </method>
    <method name="OpenAccountsDialog">
      <arg direction="in"  type="u" name="event_time" >
        <doc:doc>
          <doc:summary>
            Gtk current event time provided by <doc:ulink url="http://library.gnome.org/devel/gtk/stable/gtk-General.html#gtk-get-current-event-time">gtk_get_current_event_time()</doc:ulink>.  
          </doc:summary>
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
	    Opens stock accounts dialog for all account types.
          </doc:para>
        </doc:description>
      </doc:doc>
    </method>
    <method name="OpenAccountsDialogWithTypes">
      <arg direction="in"  type="as" name="account_types" >
        <doc:doc>
          <doc:summary>
            A list of account types that should be included in the dialog.
          </doc:summary>
        </doc:doc>  
      </arg>
      <arg direction="in"  type="u" name="event_time" >
        <doc:doc>
          <doc:summary>
            Gtk current event time provided by 
            <doc:ulink url="http://library.gnome.org/devel/gtk/stable/gtk-General.html#gtk-get-current-event-time">
            gtk_get_current_event_time()</doc:ulink>.  
          </doc:summary>
        </doc:doc>  
      </arg>
      <doc:doc>
	<doc:description>
	  <doc:para>
	    Opens stock accounts dialog for the given account types.
          </doc:para>
        </doc:description>
      </doc:doc>
    </method>
  </interface>
</node>
