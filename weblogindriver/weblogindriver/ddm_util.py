from ddm import DataModel

def get_baseurl():
    ## first we prefer the base url from the model we're actually using.
    ## this happens when we just connect to "org.freedesktop.od.Engine"
    ## and don't know in advance whether a dogfood or production or whatever
    ## server instance owns that bus name.
    ## Note that this is _supposed_ to work offline as well - the od.Engine
    ## is supposed to have an offline mode.
    model = DataModel()
    if model.global_resource:
        try:
            return model.global_resource.webBaseUrl
        except AttributeError:
            pass

    ## we fall back to a hardcoded URL, since it's probably better
    ## than crashing and would normally be right in production, but never
    ## right when testing on dogfood.
    return "http://online.gnome.org"

